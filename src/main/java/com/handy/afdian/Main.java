package com.handy.afdian;

import cn.hutool.json.JSONUtil;
import com.handy.afdian.http.AfDian;
import com.handy.afdian.param.AfDianResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试
 *
 * @author handy
 */
public class Main {

    private final static String TEST_USER_ID = "请输入你的user_id";
    private final static String TEST_TOKEN = "请输入你的token";

    public static void main(String[] args) {
        // test参数
        Map<String, Object> params = new HashMap<>();
        params.put("page", 1);

        AfDian afDianRequest = new AfDian(TEST_USER_ID, TEST_TOKEN);
        // 测试ping
        AfDianResponse ping = afDianRequest.ping(params);
        System.out.println(JSONUtil.toJsonStr(ping));
        // 查询订单
        AfDianResponse queryOrder = afDianRequest.queryOrder(params);
        System.out.println(JSONUtil.toJsonStr(queryOrder));
        // 查询赞助者
        AfDianResponse querySponsor = afDianRequest.querySponsor(params);
        System.out.println(JSONUtil.toJsonStr(querySponsor));
    }

}
