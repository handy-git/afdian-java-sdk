package com.handy.afdian.webhook;

import java.io.Serializable;

/**
 * 爱发电WebHook参数
 *
 * @author handy
 */
public class AfDianWebHookParam implements Serializable {
    /**
     * 状态码 200成功
     */
    private String ec;
    /**
     * 消息
     */
    private String em;

    /**
     * data参数
     */
    private AfDianWebHookDataParam data;
}
