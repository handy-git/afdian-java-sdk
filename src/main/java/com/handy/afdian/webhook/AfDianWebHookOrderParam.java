package com.handy.afdian.webhook;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 订单参数
 *
 * @author handy
 */
@Data
public class AfDianWebHookOrderParam implements Serializable {

    private String out_trade_no;
    private String user_id;
    private String plan_id;
    private Integer month;
    private String total_amount;
    private String show_amount;
    private Integer status;
    private String remark;
    private String redeem_id;
    private Integer product_type;
    private String discount;
    private List<String> sku_detail;
    private String address_person;
    private String address_phone;
    private String address_address;
}
