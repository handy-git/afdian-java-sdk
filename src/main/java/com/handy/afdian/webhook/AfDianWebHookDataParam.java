package com.handy.afdian.webhook;

import lombok.Data;

import java.io.Serializable;

/**
 * 爱发电hook data参数
 *
 * @author handy
 */
@Data
public class AfDianWebHookDataParam implements Serializable {

    /**
     * 类型
     */
    private String type;
    /**
     * 订单
     */
    private AfDianWebHookOrderParam order;
}
