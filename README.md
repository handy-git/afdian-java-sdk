# afdian-java-sdk

#### 介绍
这是基于java对于 [爱发电](https://afdian.net/p/010ff078177211eca44f52540025c377)
的api封装  
如果本项目帮助到您，请点个star感谢

### 依赖
本项目依赖于 [hutool](https://gitee.com/dromara/hutool) 和 lombok

#### 安装教程

1. 复制代码到你项目
2. maven导入本项目
3. fork本项目进行修改

```
<repositories>
    <repository>
        <id>gitee</id>
        <url>https://gitee.com/handy-git/afdian-java-sdk/repo/master</url>
    </repository>
</repositories>
```

#### 使用说明

1.  查看Main类即可

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


